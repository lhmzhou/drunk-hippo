package hippo

import hippo.Global.sConflictItemMap
import hippo.Global.sDocumentListenerMap
import hippo.Global.sIsHighlightMap
import hippo.tool.DocumentTools.showConflict
import com.intellij.openapi.editor.Editor
import com.intellij.openapi.editor.event.DocumentEvent
import com.intellij.openapi.editor.event.DocumentListener

class InDocumentListener(private val mEditor: Editor) : DocumentListener {

    override fun documentChanged(event: DocumentEvent) {
        val hasConflict = showConflict(mEditor)
        if (!hasConflict) {
            val document = mEditor.document
            sIsHighlightMap[mEditor] = false
            sConflictItemMap.remove(document)
            val listener = sDocumentListenerMap[document]
            if (listener != null) {
                document.removeDocumentListener(listener)
                sDocumentListenerMap.remove(document)
            }
        }
    }
}
