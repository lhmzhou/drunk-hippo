package hippo

import hippo.model.PersistentState
import hippo.setting.SettingsService
import com.intellij.openapi.components.ServiceManager

object GlobalSettings {

    fun getPersistentState(): PersistentState {
        val service = ServiceManager.getService(SettingsService::class.java)
        return service.stateValue
    }
}
