package hippo

import hippo.model.ConflictItem
import hippo.model.MarkColor
import com.intellij.openapi.editor.Document
import com.intellij.openapi.editor.Editor

object Global {
    // is it highlighted
    val sIsHighlightMap: MutableMap<Editor, Boolean> = mutableMapOf()

    // conflict item list
    val sConflictItemMap: MutableMap<Document, MutableList<ConflictItem>> = mutableMapOf()

    // document listener map
    val sDocumentListenerMap: MutableMap<Document, InDocumentListener> = mutableMapOf()

    // current color scheme
    var sCurrentColor: MarkColor = MarkColor()
        get() {
            val state = GlobalSettings.getPersistentState()
            return state.markColors[state.schemeName]!!
        }
}
