package hippo.model

import hippo.BuiltInColor

data class PersistentState(
    var schemeName: String = BuiltInColor.DEFAULT_SCHEME_NAME,
    var markColors: LinkedHashMap<String, MarkColor> = LinkedHashMap(
        mutableMapOf(
            BuiltInColor.AUTO_SCHEME_NAME to BuiltInColor.AUTO,
            BuiltInColor.INTELLIJ_SCHEME_NAME to BuiltInColor.INTELLIJ,
            BuiltInColor.DARCULA_SCHEME_NAME to BuiltInColor.DARCULA
        )
    )
)
