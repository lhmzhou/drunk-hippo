package hippo.model

data class ConflictItem(
    var currentChangeLineNum: Int = -1,
    var separatorLineNum: Int = -1,
    var incomingLineNum: Int = -1,
    var currentChangeStr: String = "",
    var incomingChangeStr: String = ""
) {
    constructor(currentChangeLineNum: Int, separatorLineNum: Int, incomingLineNum: Int) :
        this(currentChangeLineNum, separatorLineNum, incomingLineNum, "", "")
}
