package hippo.action.operation

import com.intellij.openapi.actionSystem.AnActionEvent
import com.intellij.openapi.actionSystem.CommonDataKeys

class AcceptBothChangesAction : AbsFixConflict() {
    
    override fun actionPerformed(e: AnActionEvent) {
        val editor = e.getRequiredData(CommonDataKeys.EDITOR)
        val project = e.project
        fixConflict(editor, project, ACCEPT_BOTH)
    }
}