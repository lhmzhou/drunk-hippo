package hippo.action.operation

@Retention(AnnotationRetention.SOURCE)
@Target(AnnotationTarget.CLASS)
annotation class IntDef(
    /** Defines the allowed constants for this element  */
    val value: IntArray
)
