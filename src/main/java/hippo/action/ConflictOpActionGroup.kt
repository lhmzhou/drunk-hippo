package hippo.action

import hippo.Global.sIsHighlightMap
import com.intellij.openapi.actionSystem.AnActionEvent
import com.intellij.openapi.actionSystem.CommonDataKeys
import com.intellij.openapi.actionSystem.DefaultActionGroup

class ConflictOpActionGroup : DefaultActionGroup() {

    override fun update(e: AnActionEvent) {
        // Get required data keys
        val project = e.project
        val editor = e.getData(CommonDataKeys.EDITOR)
        // Set visibility only in case of existing project and editor
        e.presentation.isVisible = false
        val canShow = project != null && editor != null
        if (canShow) {
            val isHighlight: Boolean = sIsHighlightMap.getOrDefault(editor, false)
            if (isHighlight) {
                e.presentation.isVisible = true
            }
        }
    }
}