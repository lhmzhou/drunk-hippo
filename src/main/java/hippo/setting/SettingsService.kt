package hippo.setting

import hippo.model.PersistentState
import com.intellij.openapi.components.PersistentStateComponent
import com.intellij.openapi.components.State
import com.intellij.openapi.components.Storage
import com.intellij.util.xmlb.XmlSerializerUtil

@State(
    name = "GitConflict",
    storages = [Storage(value = "hippo.1.applicationConfigurable.xml")]
)
class SettingsService : PersistentStateComponent<PersistentState?> {
    var stateValue: PersistentState = PersistentState()

    override fun getState(): PersistentState? {
        return stateValue
    }

    override fun loadState(state: PersistentState) {
        XmlSerializerUtil.copyBean(state, stateValue)
    }

    override fun noStateLoaded() {
        val state = PersistentState()
        loadState(state)
    }
}
