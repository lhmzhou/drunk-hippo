package hippo.tool

import hippo.Constants
import com.intellij.notification.Notification
import com.intellij.notification.NotificationType
import com.intellij.notification.Notifications

object NotificationTools {
    fun showNotification(title: String, content: String, type: NotificationType) {
        val notification = Notification(
            Constants.Resource.NOTIFICATION_GROUP_DISPLAY_ID, title, content, type
        )
        Notifications.Bus.notify(notification)
    }
}